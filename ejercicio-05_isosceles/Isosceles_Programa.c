#include<stdio.h>

int main(){
	float lado_A = 0, lado_B = 0, perimetro = 0;
	
	printf("Ingresa el lado A del triangulo isosceles: ");
	scanf("%f", &lado_A);
	printf("Ingresa el lado B del triangulo isosceles: ");
	scanf("%f", &lado_B);
		perimetro = (2 * lado_A) + lado_B;	
	printf("El perimetro del triangulo isosceles es: %.2f", perimetro); 
	
	return 0;
}
