#include<stdio.h>

int main(){
	int contador = 0;
	float residuo = 0;
	printf("Del 0 al 100 estos son numeros pares: \n");
	while(contador >= 0 && contador <= 100){
		residuo = contador % 2;	
		if(residuo == 0){
			printf("%i \n", contador);
		}
		contador++;
	}
	return 0;
}
