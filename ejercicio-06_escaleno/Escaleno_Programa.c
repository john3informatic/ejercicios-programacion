#include<stdio.h>

int main(){
	float lado_A = 0, lado_B = 0, lado_C = 0, perimetro = 0;
	
	printf("Ingresa el lado A: ");
	scanf("%f", &lado_A);
	printf("Ingresa el lado B: ");
	scanf("%f", &lado_B);
	printf("Ingresa el lado C: ");
	scanf("%f", &lado_C);
	
		perimetro = lado_A + lado_B + lado_C;
		
	printf("El perimetro del triangulo escaleno es: %.2f", perimetro);
			
	return 0;
}
