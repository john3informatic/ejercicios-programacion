#include<iostream>
#include<conio.h>
#include<time.h>
#include <stdlib.h>
using namespace std;

int main(){
	int numeroGenerado = 0, numeroIngresado = 0, contador = 0;
	srand(time(NULL));
	cout<<"Instrucciones: Deberas ingresar un numero para adivinar un numero del 1 al 100"<<endl; 
	cout<<"que es generado aleatoriamente por el programa,"<<endl;
	cout<<"solo tendras 5 intentos para adivinar el numero."<<endl;
	
	numeroGenerado = 1+rand()%(101-1);
	
	do{
		contador++;
		cout<<"Ingresa el numero: ";
		cin>>numeroIngresado;
			if(numeroIngresado != numeroGenerado && contador < 5){
				cout<<"Fallaste, intentalo de nuevo"<<endl;	
			}
	}while(contador != 5 && numeroIngresado != numeroGenerado);
	
	if(numeroIngresado == numeroGenerado){
		cout<<"GANASTE"<<endl;
		cout<<"El numero correcto es: "<<numeroGenerado;
	} else {
		cout<<"PERDISTE"<<endl;
		cout<<"El numero era: "<<numeroGenerado;		
	}
	
	getch();
	return 0;
}
