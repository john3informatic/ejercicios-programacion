#include<iostream>
#include<conio.h>
#include<time.h>
#include<stdlib.h>
#include<string.h>
using namespace std;

int main(){
	int dineroCpu = 500, dineroJugador = 500, voladoElegido = 0, voladoGenerado = 0, apuestaCpu = 0, apuestaJugador = 0;
	srand(time(NULL));
	
	cout<<"JUEGO DE LOS VOLADOS"<<endl;
	cout<<"Instrucciones: El juego tendra 3 rondas, al inicio la computadora realiza su apuesta entre $20 y $100,"<<endl; 
	cout<<" despues tu realizaras tu apuesta tambien entre $20 y 100,\n a continuacion eligiras AGUILA o SOL,"<<endl;
	cout<<" el programa lanzara la moneda e imprimira el resultado,\n tu ganas si adivinaste el resultado de lo contrario la computadora ganara,"<<endl;
	cout<<" el ganador se llevara la cantidad de dinero que haya apostado su rival\n y este se restara de su bolsa el dinero que perdio,"<<endl;
	cout<<" tanto tu como la computadora empiezan con una bolsa de $500. El que adivine mas rondas gana."<<endl;
	
	for(int i=1; i<=3; i++){
		cout<<"\nRONDA "<<i<<endl;
		apuestaCpu = 20+rand()%(101-20);
		cout<<"La computadora apuesta: "<<apuestaCpu<<endl;
		
			do{
				cout<<"Ingresa tu apuesta (entre $20 y $100): ";
				cin>>apuestaJugador;
			}while(apuestaJugador < 20 || apuestaJugador > 100);
			
			do{
				cout<<"Elige: "<<endl;
				cout<<"1) AGUILA"<<endl;
				cout<<"2) SOL"<<endl;
				cout<<"Numero de la opcion ingresada: ";
				cin>>voladoElegido;
			}while(voladoElegido != 1 && voladoElegido != 2);
		
		voladoGenerado = 1+rand()%(3-1);
		
			if(voladoGenerado == 1){
				cout<<"\nEl resultado fue: AGUILA"<<endl;
			} else {
			    cout<<"\nEl resultado fue: SOL"<<endl;
			}
			
			if(voladoGenerado == voladoElegido){
				cout<<"Ganaste esta ronda"<<endl;
				dineroJugador += apuestaCpu;
				dineroCpu -= apuestaCpu; 
			} else {
				cout<<"Perdiste esta ronda"<<endl;
				dineroCpu += apuestaJugador;
				dineroJugador -= apuestaJugador; 
			}
	}
	
	if(dineroJugador > dineroCpu){
		cout<<"\nGano el juegador"<<endl;
		cout<<"Dinero del jugador: "<<dineroJugador<<endl;
		cout<<"Dinero de la computador: "<<dineroCpu<<endl;
	} else if(dineroJugador == dineroCpu) {
		cout<<"\nHubo un empate"<<endl;
		cout<<"Dinero del jugador: "<<dineroJugador<<endl;
		cout<<"Dinero de la computador: "<<dineroCpu<<endl;
	} else {
		cout<<"\nGano la computadora"<<endl;
		cout<<"Dinero del jugador: "<<dineroJugador<<endl;
		cout<<"Dinero de la computador: "<<dineroCpu<<endl;
	}
	
	getch();
	return 0;
}
