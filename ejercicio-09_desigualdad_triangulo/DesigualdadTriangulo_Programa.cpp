#include<iostream>
#include<conio.h>
using namespace std;

int main(){
	float lado_A = 0, lado_B = 0, lado_C = 0;
	cout<<"Instrucciones: Ingrese la longitud de cada uno de los tres segmentos para saber si se puede formar un triangulo."<<endl;
	cout<<"Ingrese la longitud del primer segmento: ";
	cin>>lado_A;
	cout<<"Ingrese la longitud del segundo segmento: ";
	cin>>lado_B;
	cout<<"Ingrese la longitud del tercer segmento: ";
	cin>>lado_C;
	
	if((lado_A + lado_B) > lado_C && (lado_A + lado_C) > lado_B && (lado_B + lado_C) > lado_A ){
		cout<<"Es posible formar un triangulo"<<endl;
	} else
	{
		cout<<"No es posible formar un triangulo"<<endl;
	}
	
} 
