#include<iostream>
#include<conio.h>
using namespace std;

void menu();
void equilatero();
void isosceles();
void escaleno();

float lado_A = 0, lado_B = 0, lado_C = 0, perimetro = 0;
 
int main(){
	
	menu();
		
	getch();
	return 0;
}

//Funcion del Men� del programa
void menu(){
	int optionSelect;
	cout<<"Tipos de triangulos: "<<endl;
	cout<<"1) Equilatero: La longitud de todos sus lado son igaules (3A)"<<endl;
	cout<<"2) Isosceles: La longitud de dos de sus lado son iguales y el tercero diferente (2A + B)"<<endl;
	cout<<"3) Escaleno: La longitud de todos sus lados son diferentes (A + B + C)\n"<<endl;
	cout<<"Ingrese la opcion:  ";
	cin>>optionSelect;
	
	switch(optionSelect){
		case 1: equilatero(); break;
		case 2: isosceles(); break;
		case 3: escaleno(); break;
		default: cout<<"Opcion no encontrada"; break;
	}
}

//Funcion del perimetro del triangulo equilatero
void equilatero(){
	cout<<"Ingresa la longitud de uno de los lados del triangulo: ";
	cin>>lado_A;
		perimetro = 3 * lado_A;
	cout<<"El perimetro es: "<<perimetro<<endl;
}

//Funcion del perimetro del triangulo isosceles
void isosceles(){
	cout<<"Ingresa la longitud del lado A del triangulo: ";
	cin>>lado_A;
	cout<<"Ingresa la longitud del lado B del triangulo: ";
	cin>>lado_B;
		perimetro = (2 * lado_A) + lado_B;
	cout<<"El perimetro del triangulo es: "<<perimetro<<endl;
}

//Funcion del perimetro del triangulo escaleno
void escaleno(){
	cout<<"Ingresa la longitud del lado A del triangulo: ";
	cin>>lado_A;
	cout<<"Ingresa la longitud del lado B del trinagulo: ";
	cin>>lado_B;
	cout<<"Ingresa la longitud del lado C del triangulo: ";
	cin>>lado_C;
		perimetro = lado_A + lado_B + lado_C;
	cout<<"El perimetro del triangulo es: "<<perimetro<<endl;
}
