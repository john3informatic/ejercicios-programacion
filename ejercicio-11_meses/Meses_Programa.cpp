#include<iostream>
#include<conio.h>
using namespace std;

int main(){
	int mes = 0;
	
	cout<<"Ingresa un numero del 0 al 11: ";
	cin>>mes;
	
	switch(mes){
		case 0 : cout<<"El mes es enero"; break;
		case 1 : cout<<"El mes es febrero"; break;
		case 2 : cout<<"El mes es marzo"; break;
		case 3 : cout<<"El mes es abril"; break;
		case 4 : cout<<"El mes es mayo"; break;
		case 5 : cout<<"El mes es junio"; break;
		case 6 : cout<<"El mes es julio"; break;
		case 7 : cout<<"El mes es agosto"; break;
		case 8 : cout<<"El mes es septiembre"; break;
		case 9 : cout<<"El mes es octrubre"; break;
		case 10 : cout<<"El mes es noviembre"; break;
		case 11 : cout<<"El mes es diciembre"; break;
		default : cout<<"El numero ingresado es invalido";
	}
	
	getch();
	return 0;
}
