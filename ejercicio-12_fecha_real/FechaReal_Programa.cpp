#include<iostream>
#include<conio.h>
#include<string.h>
using namespace std;

int main(){
	int dia = 0, anio = 0;
	char mes[11];
	
	cout<<"Ingresa el dia: ";
	cin>>dia;
	fflush(stdin);
	cout<<"Ingresa el mes: ";
	cin.getline(mes,11,'\n');
	cout<<"Ingresa el anio: ";
	cin>>anio;
	
	if(strcmp(mes, "enero")==0 || strcmp(mes, "marzo")==0 || strcmp(mes, "mayo")==0 || strcmp(mes, "julio")==0 || strcmp(mes, "agosto")==0 || strcmp(mes, "octubre")==0 || strcmp(mes, "diciembre")==0){
		if(dia>0 && dia <= 31){
			cout<<"Es fecha real";
		} else {
			cout<<"No es fecha real";
		}
	} else if(strcmp(mes, "abril")==0 || strcmp(mes, "junio")==0 || strcmp(mes, "septiembre")==0 || strcmp(mes, "noviembre")==0){
		if(dia>0 && dia <= 30){
			cout<<"Es fecha real";
		} else {
			cout<<"No es fecha real";
		}
	} else if(strcmp(mes, "febrero")==0){
		if(anio % 4 == 0 && anio % 100 != 0 || anio % 400 == 0){
			if(dia>0 && dia <=29){
				cout<<"Es fecha real";
			} else {
				cout<<"No es fecha real";
			}
		} else {
			if(dia>0 && dia <= 28){
				cout<<"Es fecha real";
			} else {
				cout<<"No es fecha real";
			}
		}
	} else {
		cout<<"No es fecha real";
	}
	
	getch();
	return 0;
}
